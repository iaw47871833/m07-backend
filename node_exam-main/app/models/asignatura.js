var mongoose = require("mongoose");
    Schema = mongoose.Schema;

var asignaturaSchema = new Schema({
    nombre: {type: String},
    numHoras: {type: String},
    docente: {type: String},
    alumnos: {type: String},
    created_at: {type: Date, default: Date.new}
})

module.exports = mongoose.model("Asignatura", asignaturaSchema);