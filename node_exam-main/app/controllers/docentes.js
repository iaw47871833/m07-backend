var mongoose = require("mongoose"),
    Docente = require("../models/docente");

exports.listarDocentes = async(req, res, next) => {
    try {
        var result = await Docente.find({});
        if (req.isApi) {
            res.status(200).jsonp(result);
        } else {
            return result;
        }
    } catch {
        console.log("Couldn't fetch docentes ${error}");
    }
}