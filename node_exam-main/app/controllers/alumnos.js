var mongoose = require("mongoose"),
    Alumno = require("../models/alumnos");

exports.listarAlumnos = async(req, res, next) => {
    try {
        var result = await Alumno.find({});
        if (req.isApi) {
            res.status(200).jsonp(result);
        } else {
            return result;
        }
    } catch {
        console.log("Couldn't fetch alumno ${error}");
    }
}