var mongoose = require("mongoose"),
    Asignatura = require("../models/asignatura");

exports.listAsignaturas = async(req, res, next) => {
    try {
        var result = await Asignatura.find({});
        if (req.isApi) {
            res.status(200).jsonp(result);
        } else {
            return result;
        }
    } catch {
        console.log("Couldn't fetch asignatuas ${error}");
    }
}

exports.addAsignatura = (req, res, next) => {
    var asignatura = new Asignatura(req.body);
    asignatura.save((err, res) => {
        if (err) console.log(err);
        console.log("asignatura insertada");
        return res;
    })
}

exports.updateAsignatura = (req, res, next) => {
    return res.status(200).send({
        id: 3,
        nombre: "Bases de datos",
        numHoras: "120",
        docente: {
            rel: "docente",
            method: "GET",
            href: "http://localhost:3000/api/docentes/1"
        },
        alumnos: [
            {
                rel: "alumno",
                method: "GET",
                href: "http://localhost:3000/api/alumnos/1"
            },
            {
                rel: "alumno",
                method: "GET",
                href: "http://localhost:3000/api/alumnos/2"
            }
        ]
    })
}

exports.deleteAsignatura = (req, res, next) => {
    return res.status(200).send({
        id: 3,
        nombre: "Bases de datos",
        numHoras: "120",
        docente: {
            rel: "docente",
            method: "GET",
            href: "http://localhost:3000/api/docentes/1"
        },
        alumnos: [
            {
                rel: "alumno",
                method: "GET",
                href: "http://localhost:3000/api/alumnos/1"
            },
            {
                rel: "alumno",
                method: "GET",
                href: "http://localhost:3000/api/alumnos/2"
            }
        ]
    })
}