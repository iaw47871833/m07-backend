var path = require("path");
var ctrlDir = "/app/app/controllers";
var express = require("express");
const router = require("./app");

router.use(function timeLog(req, res, next) {
    req.isApi = true;
    next();
});

// Controladores
var ctrlAlumnos = require(path.join(ctrlDir, "alumnos"));
// Docentes Controller
var ctrlDocentes = require(path.join(ctrlDir, "docentes"));
// Asignatura Controller
var ctrlAsignaturas = require(path.join(ctrlDir, "asignaturas"));

// // Rutas alumnos
router.get("/alumnos", ctrlAlumnos.listarAlumnos);

// // Rutas docentes
router.get("/docentes", ctrlDocentes.listarDocentes);

// Rutas asignaturas
router.get("/asignaturas", ctrlAsignaturas.listAsignaturas);
router.post("/asignaturas/save", (req, res) => {
    const result = ctrlAsignaturas.addAsignatura(req, res);
    res.send(req.body);
});
router.put("/asignaturas/:id", ctrlAsignaturas.updateAsignatura);
router.delete("/asignaturas/:id", ctrlAsignaturas.deleteAsignatura);

module.exports = router;

// router.post("/addAsignatura", (req, res) => {
//     const result = ctrlAsignaturas.addAsignatura(req, res);
//     res.send("asignatura guardado");
// });