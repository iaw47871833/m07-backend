var path = require("path");
var express = require("express");
var router = express.Router();
var ctrlDir = "/app/app/controllers";

// Controladores
var alumnoCtrl = require(path.join(ctrlDir, "alumnos"));
var docenteCtrl = require(path.join(ctrlDir, "docentes"));
// var asignaturaCtrl = require(path.join(ctrlDir, "asignaturas"));

router.get("/", function(req, res, next) {
    res.render("chat", {title: "Chat"});
});

router.get("/asignatura/new", async function(req, res, next) {
    var docente = await docenteCtrl.listarDocentes(req, res, next);
    var alumnos = await alumnoCtrl.listarAlumnos(req, res, next);
    console.log(alumnos);
    res.render("newAsignatura", {docentes: docente, alumnos: alumnos});
});


module.exports = router;