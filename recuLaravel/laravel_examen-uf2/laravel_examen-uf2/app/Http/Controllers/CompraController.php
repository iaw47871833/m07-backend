<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Requests\CompraRequest;

class CompraController extends Controller
{

    public function main(Request $request)
    {
        /*Recuerda el estado de la compra y redirige a la pantalla en la que el usuario estaba antes: resumen, envio o confirmar */
        $estado = $request->session()->get('estado');
        // Si se ha añadido un nuevo producto, redirigimos a la pagina de resumen
        if ($request->session()->get('estado') == '') {
            return redirect('/compra/resumen');
        }
        return redirect('/compra/' . $estado);
    }

    /**
     * Metodo para añadir un producto al carrito
     */
    public function addCarrito(Request $request)
    {
        // Ponemos el valor de la sesion estado a nulo en el caso de que vuelva a añadir producto
        $request->session()->put('estado', '');
        $productos = $request->session()->get('carrito', []);
        $cantidad = (int) $request->input('qty');
        // Añadimos por la cantidad introducida
        for ($i = 0; $i < $cantidad; $i++) {
            array_push($productos, (object) $request->all());
        }
        $request->session()->put('carrito', $productos);
        //$request->session()->flush();
        return redirect(url()->previous());
    }

    // Funcion para recoger la cantidad de productos del carrito
    public function carrito(Request $request) {
        return sizeof(app('request')->session()->get('carrito',[]));
    }


    /**
     * Method to show the resume of the products in the chart
     */
    public function resumen(Request $request)
    {
        // Establecemos la sesion
        $request->session()->put('estado', 'resumen');
        //Dummy: hay que cambiar la info por la información guardada en el carrito (session)
        $products = [
            /*
            (object) [
                'name' => 'Lego 1', 'category' => 0, 'description' => 'Esto es la descripcion lego 1', 'price' => 10.02, 'image' => 'lego1.jpeg', 'rating' => 2
            ]*/];
        foreach (session()->get('carrito') as $param) {
            array_push($products, (object) $param);
        }
        return view('compra/resumen')
            ->with('products', $products);
    }

    /**
     * Method to show and process the shipping form (envio)
     */
    public function envio(Request $request)
    {
        // Establecemos la sesion
        $request->session()->put('estado', 'envio');
        return view('compra/envio');
    }
    /**
     * Method to show and process the shipping form (envio)
     */
    public function verificarEnvio(Request $request)
    {
        $formOK = false;

        /* PONER AQUI TODO LO NECESARIO PARA VERIFICAR EL FORMULARIO */
        /*Una vez verificado se guarda la información de envio en la session*/
        $this->validate($request, [
            "name" => 'required',
            "email" => 'required',
            "direccion" => 'required',
            "password" => 'required',
            "password-confirm" => 'required',
            "foto" => 'required'
        ]);
        if ($request) {
            $formOK = true;
            $shipping = $request->session()->get('shipping', []);
            // name, email, direccion, password, confirmpassword, foto
            $file = $request->file('foto');
            $destinationPath = public_path().'/img/users';
            $originalFile = $file->getClientOriginalName();
            var_dump("file". $file);
            $file->move($destinationPath, time().$originalFile);
            $cliente = [
                'name' => $request->input('name'),
                'adress' => $request->input('email'),
                'direccion' => $request->input('direccion'),
                'image' => $originalFile
            ];
            var_dump($cliente);
            $request->session()->put('shipping', $cliente);
        }
        //si el formulario se ha rellenado correctamente se redirecciona a la pagina de confirmación
        if ($formOK) {
            return redirect('/compra/confirmar');
        }
        return view('/compra/envio');
    }
    /**
     * Method to show the list of procuts and shipping info
     */
    public function confirmar(Request $request)
    {
        // Establecemos la sesion
        $request->session()->put('estado', 'confirmar');
        //Dummy: hay que cambiar la info por la información guardada en session
        $products = [
            /*
            (object) [
                'name' => 'Lego 1', 'category' => 0, 'description' => 'Esto es la descripcion lego 1', 'price' => 10.02, 'image' => 'lego1.jpeg', 'rating' => 2
            ]*/];
        foreach (session()->get('carrito') as $param) {
            array_push($products, (object) $param);
        }
        //Dummy: hay que cambiar la info por la información guardada en session
        $shipping = [   /*(object) [
            
            'name' => 'Pedro', 
            'mail' => 'asds|@asda.es',
            'address' => 'asds|@asda.es',
            'image' => 'lego1.jpeg',
        */];

        foreach (session()->get('shipping') as $param) {
            array_push($shipping, $param);
        }

        return view('compra/confirmar')->with('products', $products)->with('shipping', $shipping);
    }

    // Funcion para finalizar compra
    public function finCompra(Request $request) {
        // Vaciamos el carrito
        $request->session()->put('carrito', []);
        return view('compra/finCompra');
    }
}
