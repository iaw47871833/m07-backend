<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompraController;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'ProductController@all');
Route::get('/category/{id?}', 'ProductController@category');
Route::post('/rating', 'ProductController@rating');

Route::get('/compra', 'CompraController@main');
Route::get('/compra/resumen', 'CompraController@resumen');
Route::get('/compra/envio', 'CompraController@envio');
Route::post('/compra/envio', 'CompraController@verificarEnvio');
Route::get('/compra/confirmar', 'CompraController@confirmar');
// RUta para añadir al carrito
Route::post('/compra/addCarrito', [CompraController::class, 'addCarrito']) ->name('compra.addCarrito');
// Ruta para saber la cantidad de productos en el carrito
Route::post('/compra/carrito', [CompraController::class, 'carrito']);
// Ruta para saber si hay stock disponible
Route::post('/productos/stock', [ProductController::class, 'stock']);
// Ruta gracias por comprar
Route::get('/compra/finCompra', [CompraController::class, 'finCompra']);