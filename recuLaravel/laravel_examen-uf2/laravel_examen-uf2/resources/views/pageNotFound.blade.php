<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Not found</title>
</head>
<body>
    <div style="text-align: center;">
        <h1>ERROR...</h1>
        <h3>Lo siento, la página a la que quieres acceder no existe :(</h3>
    </div>
</body>
</html>