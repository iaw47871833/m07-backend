@extends('layouts.app')

@section('content')
<div class="row">

  <div class="col-lg-12" style="height: 600px">
    <div class="mt-5 pt-3">
        <h3 class="text-center">Gracias por realizar tu compra</h3>
        <h4 class="text-center">Tu pedido esta en proceso de envio</h4>
    </div>
  </div>
  <a href="{{ url('/') }}" class="btn btn-primary btn-lg">Volver a la página principal</a>
  <!-- /.col-lg-12 -->

  
</div>


@endsection