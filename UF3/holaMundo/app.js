require("dotenv").config();
var port = process.env.SERVER_PORT;
var express = require("express");
var app = express();
var path = require("path")
var router = express.Router();

var server =  require("http")
    .createServer(app)
    .listen(port, () => {
        console.log("Servidor escuchando por el puerto: " + port);
    });

app.use(express.static(__dirname + "/views"));
var indexRouter = require("./app/routes/base");