require("dotenv").config();
const port = process.env.SERVER_PORT || 6000;
var express = require("express"),
  app = express(),
  server = require("http").createServer(app),
  path = require("path");

// Creamos un servicio de sockets
var io = require("socket.io")(server);


server.listen(port, (err, res) => {
  if (err) console.log(`ERROR: Connecting APP ${err}`);
  else console.log(`Server is running on port ${port}`);
});

// Import routes of our app

var socketsRouter = require("./app/routes/sockets");
var handlerError = require("./app/routes/handler");

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));



// Define routes using URL path
app.use("/", socketsRouter);
app.use(handlerError);

/*Socket functions */
io.on("connection", (socket) => {
  console.log("Cliente conectado");
  // socket.on("connect", () => {
  //   var msgNewClient = "Se ha conectado un nuevo cliente a la sesión";
  //   socket.broadcast.emit(msgNewClient);
  // });
  // var msgNewClient = "Se ha conectado un nuevo cliente a la sesión";
  // socket.broadcast.emit("newMsg", msgNewClient);
  // socket.user="Pedro";
  // Cuando escuche algo por el canal newMsg, recibe la información y hace algo
  socket.on("newMsg", (data) => {
    console.log(data);
    var message = "I listen!";
    // Emitimos el mensaje a todos los del canal menos la persona que lo envia
    socket.broadcast.emit("newMsg", {data, message});
  });
})

module.exports = app;
