$(document).ready(function () {
  //Inicializa socket con IO
  const socket = io();

  $("#send").submit(function (e) {
    e.preventDefault();
    var msg = $("#msg").val();
    //Acciones a realizar cuando se pulsa el boton submit
    // Append del mensaje a la caja de mensajes
    $("#chat").append(`<p>${msg}</p>`);
    $("#msg").val("");
    // El socket envia un mensaje por el canal newMsg
    var toSend = {user:1, text:msg};
    socket.emit("newMsg", toSend);
  });

  //We listen on "newMsg" and react if we detect 
  //Acciones a realizar cuando se detecta actividad en el canal newMsg
  socket.on("newMsg", (data) => {
    // alert(msgNewClient);
    console.log(data);
    alert("Mensaje recibido: " + data.data.text);
    $("#chat").append(`<p>${data.data.text}</p>`);
  })
});
