<!DOCTYPE html>
<html>
    <head>
        <title>Pildora 2</title>
        <meta charset='utf-8'>
    </head>
    <body>
        <h1>Pildora 2</h1>
        <?php
            // Crea una URL en la que pasemos el nombre y la edad del usuario actual y posteriormente recuperalo con las superglobales $_GET
            $nombre = $_GET["nombre"];
            $edad = $_GET["edad"];
            echo "Tu nombre es $nombre y tienes $edad años";
        ?>

        <?php
            // Crea un formulario que envie a otra pagina los datos personales de una persona en formato POST. Escribe el codigo del script que recibe 
            // esta información para mostrarla en pantalla.
        ?>
        <form action="recibo.php" method="POST">
            <label for="nombre">
                Tu nombre:
                <input type="text" name="nombre">
            </label> <br>
            <label for="apellido">
                Tu apellido:
                <input type="text" name="apellido">
            </label> <br>
            <label for="email">
                Tu email:
                <input type="text" name="email">
            </label> <br>
            <input type="submit" value="Enviar formulario">
        </form>
    </body>
</html>