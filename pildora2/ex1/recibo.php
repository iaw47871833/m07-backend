<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Recibir datos por POST</title>
</head>
<body>
    <?php
        // Guardamos los datos por POST
        $nombre = $_POST["nombre"];
        $apellido = $_POST["apellido"];
        $email = $_POST["email"];
        // Mostramos los datos
        echo "Hola $nombre $apellido! <br>";
        echo "Te has registrado con el siguiente correo: $email";
    ?>
</body>
</html>