<?php
    // Guardamos los números y la operación
    $numero1 = $_POST["numero1"];
    $numero2 = $_POST["numero2"];
    $operacion = $_POST["operacion"];
    // Calculamos la operacion
    switch($operacion) {
      case "+":
        $resultado = $numero1 + $numero2;
        break;
      case "-":
        $resultado = $numero1 - $numero2;
        break;
      case "*":
        $resultado = $numero1 * $numero2;
        break;
      case "/":
        $resultado = $numero1 / $numero2;
        break;
      case "**":
        $resultado = $numero1 **  $numero2;
        break;
    }
    // Lo mostramos
    echo "El resultado final es: $resultado";
?>