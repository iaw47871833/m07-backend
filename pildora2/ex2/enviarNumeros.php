<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Enviar números</title>
</head>
<body>
    <h1>Operaciones aritméticas</h1>
    <form action="recibirNumeros.php" method="POST">
        <!--Input del primer número-->
        <label for="numero1">
            Numero 1:
            <input type="text" name="numero1">
        </label> <br>
        <!--Input del segundo número-->
        <label for="numero2">
        Numero 2:
            <input type="text" name="numero2">
        </label> <br>
        <!--Desplegable de operaciones-->
        <select name="operacion">
            <option value="+">Suma</option>
            <option value="-">Resta</option>
            <option value="*">Multiplicación</option>
            <option value="/">DIvisión</option>
            <option value="**">Exponente</option>
        </select>
        <input type="submit" value="Enviar">
    </form>
</body>
</html>