<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 7 Funciones</title>
</head>
<body>
    <h1>Ejercicio 7</h1>
    <?php
        /*
            Realiza una función separar(lista) que tome una lista de números enteros y devuelva 
            (no imprimir) dos listas ordenadas. La primera con los números pares y la segunda
             con los números impares. 
        */
        // Inicializamos el array
        $paresImpares = array (
          "pares" => array(

          ),
          "impares" => array(

          )
        );
        function separar($lista) {
          // Recorremos los numeros de la lista
          for ($i = 0; $i < count($lista); $i++) {
            // Si el modulo del número entre 2 es igual a 0, lo añadimos a pares
            // En caso contrario lo añadimos a impares
            if ($lista[$i] % 2 == 0) {
              $paresImpares["pares"][] = $lista[$i];
            } else {
              $paresImpares["impares"][] = $lista[$i];
            }
          }
          //return $paresImpares;
          // Mostramos todos los numeros
          echo "numeros = [ ";
          foreach($lista as $numero) {
            echo $numero. ", ";
          }
          echo " ]<br>";
          // Mostramos los pares e impares
          echo "Resultado:<br>";
          echo "[ ";
          foreach($paresImpares["pares"] as $par) {
            echo $par. ", ";
          }
          echo " ]<br>";
          echo "[ ";
          foreach($paresImpares["impares"] as $impar) {
            echo $impar. ", ";
          }
          echo " ]";

        }
        // Guardamos la lista
        $listaNumeros = $_POST["numeros"];
        // Llamamos a la función
        separar($listaNumeros);
    ?>
</body>
</html>