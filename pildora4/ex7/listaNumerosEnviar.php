<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 7 Funciones</title>
</head>
<body>
    <h1>Ejercicio 7</h1>
    <?php
        // Si no se ha introducido la cantidad de numeros, mostramos el formulario
        // En caso contrario mostramos el siguiente formulario
        if (empty($_POST['cantidadNumeros'])) {
    ?>
    <form action="listaNumerosEnviar.php" method="POST">
        <label for="cantidadNumeros">
            Cuantos numeros quieres introducir:
            <input type="text" name="cantidadNumeros">
        </label> <br>
        <input type="submit" value="Enviar">
    </form>
    <?php
        } else {
    ?>
    <form action="listaNumerosRecibir.php" method="POST">
        <?php
            for ($i = 0; $i < $_POST['cantidadNumeros']; $i++) {
        ?>
        <label for="numeros">
            Introduce un numero:
            <input type="text" name="numeros[]">
        </label> <br>
        <?php
            }
        ?>
        <input type="submit" value="Enviar">
    </form>
    <?php
        }
    ?>
</body>
</html>