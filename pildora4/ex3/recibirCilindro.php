<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 3 funciones</title>
</head>
<body>
    <h1>Ejercicio 3</h1>
    <?php
        /*
            Una función que reciba como parámetros el valor
            del radio de la base y la altura de un cilindro y
            devuelva el volumen del cilindro, teniendo en cuenta 
            que el volumen de un cilindro se calcula como 
            Volumen = númeroPi * radio * radio * Altura 
            siendo númeroPi = 3.1416 aproximadamente.
        */
        function volumenCilindro ($radio, $altura) {
            return pi() * $radio * $radio * $altura;
        }
        // Guardamos los datos del usuario
        $radio = $_POST["radio"];
        $altura = $_POST["altura"];
        // Mostramos el resultado
        echo "El volumen del cilindro con radio ". $radio. " y altura ". 
        $altura. " es: ". volumenCilindro($radio, $altura);
    ?>
</body>
</html>