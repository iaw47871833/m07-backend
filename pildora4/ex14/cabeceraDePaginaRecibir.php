<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cabecera de pagina</title>
</head>
<body>
    <?php
        // Creamos la clase cabecera
        class CabeceraDePagina {
            // Atributos
            private $titulo;
            private $posicion;
            private $colorFondo;
            private $colorTexto;

            // Constructor con parametros predeterminados
            public function __construct() {
                $this->titulo = "Cabecera por defecto";
                $this->posicion = "center";
                $this->colorFondo = "#FF4500";
                $this->colorTexto = "#FFFFFF";
            }

            // Función mostrar
            public function mostrar() {
                echo "<h1 style='text-align: $this->posicion; color: $this->colorTexto; background-color: $this->colorFondo'>"
                 . $this->titulo .
                  "</h1>";
            }
        }
        // Instanciamos el objeto cabecera de pagina
        $cabecera = new CabeceraDePagina();
        // Mostramos la cabecera
        $cabecera->mostrar();
    ?>
</body>
</html>