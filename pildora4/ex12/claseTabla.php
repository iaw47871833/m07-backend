<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Clase Tabla</title>
</head>

<body>
    <h1>Tabla</h1>
    <?php
    class Tabla
    {
        private $mat = array();
        private $cantFilas;
        private $cantColumnas;

        public function __construct($fi, $co)
        {
            $this->cantFilas = $fi;
            $this->cantColumnas = $co;
        }

        public function cargar($fila, $columna, $valor)
        {
            $this->mat[$fila][$columna] = $valor;
        }

        private function inicioTabla()
        {
            echo '<table border="1">';
        }

        private function inicioFila()
        {
            echo '<tr>';
        }

        private function mostrar($fi, $co)
        {
            echo '<td>' . $this->mat[$fi][$co] . '</td>';
        }

        private function finFila()
        {
            echo '</tr>';
        }

        private function finTabla()
        {
            echo '</table>';
        }

        public function graficar()
        {
            // Inicio tabla
            $this->inicioTabla();
            // Bucle para recorrer las filas
            for ($i = 1; $i <= $this->cantFilas; $i++) {
                $this->inicioFila();
                // Bucle para mostrar los datos de cada columna
                for ($j = 1; $j <= $this->cantColumnas; $j++) {
                    $this->mostrar($i, $j);
                }
                $this->finFila();
            }
            // Fin tabla
            $this->finTabla();
        }
    }

    $tabla1 = new Tabla(2, 3);
    $tabla1->cargar(1, 1, "1");
    $tabla1->cargar(1, 2, "2");
    $tabla1->cargar(1, 3, "3");
    $tabla1->cargar(2, 1, "4");
    $tabla1->cargar(2, 2, "5");
    $tabla1->cargar(2, 3, "6");
    $tabla1->graficar();
    ?>
</body>

</html>