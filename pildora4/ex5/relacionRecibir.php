<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 5 Funciones</title>
</head>
<body>
    <h1>Ejercicio 5</h1>
    <?php
        /*
            Realiza una función llamada relacion(a, b) que a
            partir de dos números cumpla lo siguiente:
            - Si el primer número es mayor que el segundo, debe devolver 1.
            - Si el primer número es menor que el segundo, debe devolver -1.
            - Si ambos números son iguales, debe devolver un 0.
        */
        function relacion($a, $b) {
            // Inicialización de la variable relacion
            $relacion = 0;
            // Si el primer número es mayor al segundo devolvemos 1
            // Si el segundo número es mayor al primero devolvemos -1
            if ($a > $b) {
                $relacion = 1;
            } elseif ($a < $b) {
                $relacion = -1;
            }
            return $relacion;
        }
        // Guardamos los valores del usuario
        $num1 = $_POST["num1"];
        $num2 = $_POST["num2"];
        // Llamamos a la función
        echo relacion($num1, $num2);
    ?>
</body>
</html>