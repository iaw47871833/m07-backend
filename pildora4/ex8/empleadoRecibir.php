<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 1 POO</title>
</head>
<body>
    <h1>Empleado</h1>
    <?php
        // Creación de la clase Empleado
        class Empleado {
            // Atributos
            // Nombre del empleado
            private $nombre;
            // Sueldo del empleado
            private $sueldo;

            // Función inicializar
            public function inicializar($nombre, $sueldo) {
                $this->nombre = $nombre;
                $this->sueldo = $sueldo;
            }

            // Función imprimir
            public function imprimir() {
                echo $this->nombre. ", ";
                // Mostramos un mensaje si debe o no pagar impuestos (superior a 3000)
                if ($this->sueldo > 3000) {
                    echo "debes pagar impuestos";
                } else {
                    echo "no debes pagar impuestos";
                }
            }
        }
        // Guardamos los datos del usuario
        $nombreEmpleado = $_POST["nombre"];
        $sueldoEmpleado = $_POST["sueldo"];
        // Creamos el objeto
        $empleado = new Empleado();
        // Iniciamos el empleado
        $empleado->inicializar($nombreEmpleado, $sueldoEmpleado);
        // Llamamos a la función imprimir
        $empleado->imprimir();
    ?>
</body>
</html>