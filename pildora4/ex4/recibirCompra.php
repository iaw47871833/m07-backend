<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 4 Funciones</title>
</head>
<body>
    <h1>Ejercicio 4</h1>
    <?php
        /*
            Crear un formulario en el que se introduce el precio
            de una compra y realizar el programa que calcula mediante
            una función la siguientes reglas Un comercio realiza 
            descuentos con la siguiente regla:
            - Si la compra no alcanza los $100, no se realiza ningún descuento.
            - Si la compra está entre $100 y $499,99, se descuenta un 10%.
            - Si la compra supera los $500, se descuenta un 15%.
        */
        function calcularPrecio ($precio) {
            // Inicializamos el precio final con el precio dado
            // en el caso de que sea menor de 100$
            $precioFinal = $precio;
            // Caso $100 - 499.99
            if ($precio >= 100 && $precio < 500) {
                $precioFinal = $precio - $precio * 0.10;
            }
            // Caso superior a $500
            else if ($precio > 500) {
                $precioFinal = $precio - $precio * 0.15;
            }
            // Devolvemos el precio final
            return $precioFinal;
        }

        // Guardamos el precio de compra
        $precioCompra = $_POST["precio"];

        echo "El precio final de tu compra es: " + calcularPrecio($precioCompra). "$";
    ?>
</body>
</html>