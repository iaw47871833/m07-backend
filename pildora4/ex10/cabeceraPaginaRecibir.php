<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 3 POO</title>
</head>
<body>
    <?php
        // Creamos la clase cabecera
        class Cabecera {
            // Atributos
            private $titulo;
            private $posicion;
            private $colorFondo;
            private $colorTexto;

            // Función inicializar clase
            public function inicializar($titulo, $posicion, $colorFondo, $colorTexto) {
                $this->titulo = $titulo;
                $this->posicion = $posicion;
                $this->colorFondo = $colorFondo;
                $this->colorTexto = $colorTexto;
            }

            // Función mostrar
            public function mostrar() {
                echo "<h1 style='text-align: $this->posicion; color: $this->colorTexto; background-color: $this->colorFondo'>"
                 . $this->titulo .
                  "</h1>";
            }
        }

        // Guardamos los datos del usuario
        $tituloUsuario = $_POST["titulo"];
        $posUsuario = $_POST["posicion"];
        $colorFondoUsuario = $_POST["colorFondo"];
        $colorTextoUsuario = $_POST["colorTexto"];
        // Instanciamos el objeto cabecera
        $cabecera = new Cabecera();
        // Inicializamos la cabecera
        $cabecera->inicializar($tituloUsuario, $posUsuario, $colorFondoUsuario, $colorTextoUsuario);
        // Mostramos la cabecera
        $cabecera->mostrar();
    ?>
</body>
</html>