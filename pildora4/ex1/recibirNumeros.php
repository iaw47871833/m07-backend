<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 1 funciones</title>
</head>
<body>
    <h1>Ejercicio 1</h1>
    <?php
        /*
            Una función que reciba cinco números enteros como parámetros
            y muestre por pantalla el resultado de sumar los cinco números (tipo 
            procedimiento, no hay valor devuelto).
        */

        function sumaNumeros ($num1, $num2, $num3, $num4, $num5) {
            // Calculamos la suma de los parametros
            $suma = $num1 + $num2 + $num3 + $num4 + $num5;
            // Mostramos la suma
            echo "<p>El resultado es $suma</p>";
        }

        // Guardamos los números
        $num1 = $_POST["num1"];
        $num2 = $_POST["num2"];
        $num3 = $_POST["num3"];
        $num4 = $_POST["num4"];
        $num5 = $_POST["num5"];
        sumaNumeros($num1, $num2, $num3, $num4, $num5);
    ?>
</body>
</html>