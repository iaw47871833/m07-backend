<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 2 Funciones</title>
</head>
<body>
    <h1>Ejercicio 2</h1>
    <?php
        /*
            Una función que reciba cinco números enteros
            como parámetros y devuelva el resultado de sumar los
            cinco números (tipo función, hay un valor devuelto). Asigna 
            el resultado de una invocación a la función con los 
            números 2, 5, 1, 8, 10 a una variable de nombre $tmp y 
            muestra por pantalla el valor de la variable.
        */

        function sumaNumeros($num1, $num2, $num3, $num4, $num5) {
            return $num1 + $num2 + $num3 + $num4 + $num5;
        }
        // Guardamos los numeros
        $num1 = $_POST["num1"];
        $num2 = $_POST["num2"];
        $num3 = $_POST["num3"];
        $num4 = $_POST["num4"];
        $num5 = $_POST["num5"];
        // Asignamos la invocación de la función a una variable
        $tmp = sumaNumeros($num1, $num2, $num3, $num4, $num5);
        // Mostramos el valor por pantalla
        echo "El resultado es $tmp";
    ?>
</body>
</html>