<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Lista hipervinculos</title>
    <style>
        ul {
            list-style-type: none;
            display: flex;
        }

        ul li {
            background: blue;
            padding: 10px;
        }

        ul li:hover {
            background-color: #2586d7;
        }

        ul li a {
            color: white;
            font-size: 18px;
            font-family: Arial;
            text-decoration: none;
        }
    </style>
</head>
<body>
    <h1>Lista hipervínculos</h1>
    <?php
        // Creamos la clase
        class Menu {
            // Atributos
            // Enlaces del menu
            private $enlaces;
            // Funcion inicializar
            public function inicializar() {
                $this->enlaces = array(
                    "titulo" => array(

                    ),
                    "url" => array(

                    )
                );
            }
            // Funcion cargarOpcion 
            public function cargarOpcion($titulo, $url) {
                // Añadimos al array el nuevo enlace
                $this->enlaces["titulo"][] = $titulo;
                $this->enlaces["url"][] = $url;
            }
            // Función mostrar
            public function mostrar() {
                echo "<div><ul>";
                    // Bucle para recorrer el array de enlaces
                    for ($i = 0; $i < count($this->enlaces["titulo"]); $i++) {
                        // Mostramos el enlace
                        echo "<li><a href='". $this->enlaces["url"][$i] ."'>". $this->enlaces["titulo"][$i]. "</a></li>";
                    }
                echo "</ul></div>";
            }
        }
        // Creamos el objeto
        $menuEnlaces = new Menu();
        $menuEnlaces->inicializar();
        // Añadimos un enlace
        $menuEnlaces->cargarOpcion("Google", "https://www.google.com/");
        $menuEnlaces->cargarOpcion("Youtube", "https://www.youtube.com/");
        $menuEnlaces->cargarOpcion("W3Schools", "https://www.w3schools.com/");
        $menuEnlaces->cargarOpcion("Gitlab", "https://gitlab.com/");
        // Mostramos los enlaces
        $menuEnlaces->mostrar();
    ?>
</body>
</html>