<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Clase menu</title>
</head>

<body>
    <h1>Menu</h1>
    <?php
    class Menu
    {
        private $enlaces = array();
        private $titulos = array();
        public function cargarOpcion($en, $tit)
        {
            $this->enlaces[] = $en;
            $this->titulos[] = $tit;
        }
        private function mostrarHorizontal()
        {
            // Bucle para cada enlace
            for ($i = 0; $i < count($this->enlaces); $i++) {
                echo '<a href="' . $this->enlaces[$i] . '" style="margin-right: 8px;">' . $this->titulos[$i] . '</a>';
            }
        }
        private function mostrarVertical()
        {
            for ($f = 0; $f < count($this->enlaces); $f++) {
                echo '<a href="' . $this->enlaces[$f] . '">' . $this->titulos[$f] . '</a>';
                echo "<br>";
            }
        }

        public function mostrar($orientacion)
        {
            // Llamamos a los metodos mostrarHorizontal o mostrarVertical
            // dependiendo de la orientacion pasada
            if ($orientacion == "horizontal") {
                $this->mostrarHorizontal();
            } else {
                $this->mostrarVertical();
            }
        }
    }

    $menu1 = new Menu();
    $menu1->cargarOpcion('http://www.lanacion.com.ar', 'La Nación');
    $menu1->cargarOpcion('http://www.clarin.com.ar', 'El Clarín');
    $menu1->cargarOpcion('http://www.lavoz.com.ar', 'La Voz del Interior');
    $menu1->mostrar("horizontal");
    echo '<br>';
    $menu2 = new Menu();
    $menu2->cargarOpcion('http://www.google.com', 'Google');
    $menu2->cargarOpcion('http://www.yahoo.com', 'Yhahoo');
    $menu2->cargarOpcion('http://www.msn.com', 'MSN');
    $menu2->mostrar("vertical");
    ?>
</body>

</html>