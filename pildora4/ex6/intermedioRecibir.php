<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 6 Funciones</title>
</head>
<body>
    <h1>Ejercicio 6</h1>
    <?php
        /*
            Realiza una función llamada intermedio(a, b) que a partir de dos
            números, devuelva su punto intermedio. Cuando lo tengas comprueba 
            el punto intermedio entre -12 y 24
        */
        function intermedio($a, $b) {
            // Devolvemos el punto intermedio
            return ($a + $b) / 2;
        }
        // Guardamos los valores del usuario
        $num1 = $_POST["num1"];
        $num2 = $_POST["num2"];
        // Llamamos a la función
        echo "El punto intermedio entre $num1 y $num2 es: " . intermedio($num1, $num2);
    ?>
</body>
</html>