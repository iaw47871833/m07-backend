# Carpeta app/http/controllers

La carpeta ```app/http/controllers``` nos sirve como una alternativa para no tener que definir nuestras peticiones HTTP a través del archivo ```routes.php```. En esta carpeta, organizamos el comportamiento de rutas utilizando clases ```Controller```.

Los controladores pueden agrupar peticiones HTTP utilizando lógica en clases.

Un controlador trabaja con las peticiones:

* GET
* POST
* PUT
* DELETE
* PATCH

Los controladores nos ayudan a agrupar estas peticiones en una clsae que se liga a las rutas, en el archivo ```app/http/routes.php```

Por ejemplo, si tenemos el método:

```
<?php
    namespace App/Http/Controllers;

    class UserController extends Controller {
        public function index() {
            return 'Usuarios';
        }
    }
?>
```

Para poder enlazar una ruta con un controlador pasamos como argumento el nombre del controlador y del método que queremos enlazar, separados por un @.

Por ejemplo si creamos la ruta ```/usuarios```.

```
Route::get('/usuarios', 'UserController@index');
```