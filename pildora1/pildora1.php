<!DOCTYPE html>
<html>
    <head>
        <title>Pildora 1</title>
        <meta charset='utf-8'>
    </head>
    <body>
        <?php
            //1. Realiza todos los cuestionarios de la Pildora 1
            // Crea un script para muestre un texto de cabecera h1 y un parrafo por pantalla
            echo "<h1>Hello world!</h1>";
            echo "<p>Mi primer programa!</p>";

            // Crea un saludo que cambie en función del nombre almacenado en una variable $name
            $name = "Alex";
            echo "<p>Esta página web ha sido creada por $name</p>";

            // Haz un programa que realize en una misma sentencia un conjunto de diferentes 
            // operaciones aritmeticas utilizando tanto numero como valores almacenados en variables.
            $numero1 = 5.2;
            $numero2 = 2.3;
            $resultado = 2 + $numero1 * 5.5 * $numero2;
            echo "<p>El resultado de la operación es $resultado</p>";

            /*
                2. Crea una pagina en la que se almacenan dos numeros en variables y muestre en una
                    tabla los valores que toman las variables y cada uno de los resultados de todas las 
                    operaciones aritmeticas. 
            */
            // Guardamos los numeros
            $numero1 = 5;
            $numero2 = 7;
            // Operaciones
            $suma = ($numero1 + $numero2);
            $resta = ($numero1 - $numero2);
            $multiplicacion = ($numero1 * $numero2);
            $division = round(($numero1 / $numero2), 2);
            $exponente = ($numero1 ** $numero2);
        ?>
            <!--Mostramos la tabla-->
            <table>
                <tr>
                    <th>Operación</th>
                    <th>Valor</th>
                </tr>
                <tr>
                    <td>A</td>
                    <td><?php echo "$numero1"?></td>
                </tr>
                <tr>
                    <td>B</td>
                    <td><?php echo "$numero2"?> </td>
                </tr>
                <tr>
                    <td>A+B</td>
                    <td><?php echo "$suma"?> </td>
                </tr>
                <tr>
                    <td>A-B</td>
                    <td><?php echo "$resta"?> </td>
                </tr>
                <tr>
                    <td>A*B</td>
                    <td><?php echo "$multiplicacion"?> </td>
                </tr>
                <tr>
                    <td>A/B</td>
                    <td><?php echo "$division"?> </td>
                </tr>
                <tr>
                    <td>AexpB</td>
                    <td><?php echo "$exponente"?> </td>
                </tr>
            </table>
    </body>
</html>