<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Jugadores de futbol Recibir</title>
</head>
<body>
    <h1>Liga de fútbol</h1>
    <?php
        // Guardamos el número de jugadores y el número de partidos jugados
        $numJugadores = $_POST["jugadores"];
        $numPartidos = $_POST["partidos"];
        // Pedimos a los jugadores los goles que han marcado
        echo "<form action='jugadoresFutbolMostrar.php' method='POST'>";
        for ($i = 0; $i < $numJugadores; $i++) {
            
            echo "
                    Nombre:
                    <input type='text' name='jugador[][nombre]'> <br>
                ";
                // Pedimos los goles de cada partido
                for ($j = 0; $j < $numPartidos; $j++) {
                    echo "
                        Partido ". ($j + 1). ":
                        <input type='text' name='goles[". $i ."][]'>";
                    echo "<br>";
                }
        }
        // Input para guardarnos el numero de partidos para el siguiente formulario
        echo "<input type='hidden' name='partidos' value='$numPartidos'>";
        echo "<input type='submit' value='Enviar'>";
        echo "</form>";
    ?>
    
</body>
</html>