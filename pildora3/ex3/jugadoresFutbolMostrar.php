<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Jugadores futobl mostrar tabla</title>
    <style>
        table, td, th {  
        border: 1px solid #ddd;
        text-align: left;
        }

        table {
        border-collapse: collapse;
        }

        th, td {
        padding: 15px;
        }
    </style>
</head>
<body>
    <h1>Jugadores de fútbol</h1>
    <?php
        // Guardamos los jugadores y los goles marcados por cada partido
        $jugadores = $_POST["jugador"];
        $goles = $_POST["goles"];
        $partidos = $_POST["partidos"];
    ?>
    <table>
        <tr>
            <th>Jugador</th>
            <?php
                // Cabecera de los partidos
                for ($i = 0; $i < $partidos; $i++) {
                    echo "<th>Partido ". ($i + 1) . "</th>";
                }
            ?>
        </tr>
        <?php
            // Bucle para recorrer los jugadores
            for ($i = 0; $i < count($jugadores); $i++) {
                echo "<tr>";
                // Bucle para recorrer los partidos de cada jugador
                // Recorremos el número de partidos + 1 para añadir el nombre del jugador
                for ($j = 0; $j < $partidos; $j++) {
                    // Para la primera posición guardamos el nombre
                    if ($j == 0) {
                        echo "<td>". $jugadores[$i]['nombre']. "</td>";
                    }
                    echo "<td>". $goles[$i][$j]."</td>";
                }
                echo "</tr>";
            }
        ?>
    </table>
</body>
</html>
