<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 3 Control de Flujo</title>
</head>
<body>
    <!--Utilizando el array del ejercicio anterior, crea un programa que cuente el numero de
     veces que aparece un numero dado a traves de un formulario post. En la pagina del
    resultado ha de aparecer el numero que se busca y el numero de coincidencias en una frase 'bonita'-->
    <h1>Buscar número</h1>
    <form action="contarNumerosRecibir.php" method="POST">
        <label for="numero">
            Introduce un número a buscar:
            <input type="text" name="numero">
        </label>
        <input type="submit" value="Enviar">
    </form>
</body>
</html>