<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 3 Control de Flujo</title>
</head>
<body>
    <?php
        $loteria = array(61, 32, 43, 61, 61, 4, 3);
        // Inicializamos el contador a 0
        $contador = 0;
        // Recibimos el número
        $numero = $_POST["numero"];
        // Recorremos el array y comparamos los valores
        for ($i = 0; $i < count($loteria); $i++) {
            if ($loteria[$i] == $numero) {
                $contador++;
            }
        }
    ?>
    <p>El número <?phpecho "$numero"?> aparece <?php echo "$contador"?> veces.</p>
</body>
</html>