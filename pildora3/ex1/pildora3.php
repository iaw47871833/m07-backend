<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Pildora 3</title>
</head>
<body>
    <h1>Pildora 3: Control de Flujo</h1>
    <?php
        // 2.Dado el siguiente array $loteria = array(61, 32, 43, 61). Haz un programa que 
        // muestre por pantalla el numero de veces que aparece el numero 61
        // Inicializamos el array
        $loteria = array(61, 32, 43, 61);
        // Inicializamos el contador a 0
        $contador = 0;
        // Recorremos el array
        for ($i = 0; $i < count($loteria); $i++) {
            // Comparamos el valor con el valor a buscar
            if ($loteria[$i] == 61) {
                $contador++;
            }
        }
        // Mostramos las veces que aparece
        echo "El número 61 aparece $contador veces";
    ?>
</body>
</html>