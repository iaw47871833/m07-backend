<?php
namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{    
   
    private $products;
    private $_filters;
    public $filtersArray;
    public $productModel;

    public function __construct()
    {
        /**
         * Filters (name=>value) format to show in the view
         * Write the content of the stars
         */
        $this->_filters=(object)array(
            'category'=>array('Categori1'=>'cat1','Categori2'=>'cat3','Categori3'=>'cat3'),
            'stars'=>array(1, 2, 3, 4, 5)
        );
        $this->filtersArray = (array)$this->_filters;
        $this->productModel = new Product();
    }
    /**
     * Method to list all the products
     */
    public function all()
    {   
        return view('examenViews/products')->with(['products' => $this->productModel->get()]);
    }

    /**
     * Method to list the products filtered by category
     */
    public function category()
    {   
        return view('examenViews/category')->with(['products' => $this->productModel->get(),
                                                    'filters' => $this->filtersArray]);
    }

    /**
     * Method to list the products filtered by stars
     */
    public function stars()
    {
        return view('examenViews/stars')->with(['products' => $this->productModel->get(),
                                                    'filters' => $this->filtersArray]);
    }
}