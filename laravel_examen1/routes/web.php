<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [ProductController::class, 'all']);
/*Route::get('/', function () {
    return view('examenViews/products', [ProductController::class, 'all']);
})->name('home');;
*/
Route::get('/category', [ProductController::class, 'category']);

Route::get('/stars', [ProductController::class, 'stars']);

Route::get('/register', function () {
    return view('register');
})->name('register');;

Route::get('/login', function () {
    return view('login');
})->name('login');;
//Auth::routes();