<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class products_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert(array(
            'name' => 'Lego creator',
            'price' => 35.00,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut scelerisque consectetur imperdiet. In eu porttitor. ',
            'points' => 3,
            'category' => 'clasico',
            'img' => './public/img/lego1.jpeg'
        ));
        DB::table('products')->insert(array(
            'name' => 'Lego city',
            'price' => 50.00,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut scelerisque consectetur imperdiet. In eu porttitor. ',
            'points' => 5,
            'category' => 'city',
            'img' => './public/img/lego2.jpeg'
        ));
        DB::table('products')->insert(array(
            'name' => 'Lego friends',
            'price' => 45.00,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut scelerisque consectetur imperdiet. In eu porttitor. ',
            'points' => 2,
            'category' => 'especial',
            'img' => './public/img/lego3.jpeg'
        ));
        DB::table('products')->insert(array(
            'name' => 'Lego classic',
            'price' => 39.00,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut scelerisque consectetur imperdiet. In eu porttitor. ',
            'points' => 4,
            'category' => 'clasico',
            'img' => './public/img/lego4.jpeg'
        ));
    }
}
