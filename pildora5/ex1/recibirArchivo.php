<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 1 Ficheros</title>
</head>
<body>
    <h1>Ejercicio 1 Ficheros</h1>
    <?php
        /*
            Crea un formulario que puedas subir un archivo. En la pagina de
            destino ha de verse los atributos del archivo: nombre, tamaño,
            ubicación temporal, extensión…
        */
        // Comprobamos que se ha enviado un archivo
        // Si existe el archivo y no muestra ningun error, lo mostramos
        // En el caso contrario advertimos al usuario
        if (isset($_FILES["archivo"]) && $_FILES["archivo"]["error"] == 0) {
            // Guardamos el archivo
            $archivo = $_FILES["archivo"];
            // Mostramos los datos del archivo
            echo "<p>Nombre: ". $archivo["name"]. "</p>";
            echo "<p>Tamaño: ". $archivo["size"]. "</p>";
            echo "<p>Tipo de archivo: ". $archivo["type"]. "</p>";
            echo "<p>Ubicación temporal: ". $archivo["tmp_name"]. "</p>";
        } else {
            echo "<p>No se ha introducido ningun archivo</p>";
        }
        
    ?>
</body>
</html>