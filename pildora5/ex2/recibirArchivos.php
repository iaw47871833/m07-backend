<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 2 Ficheros</title>
</head>
<body>
    <h1>Ejercicio 2 Ficheros</h1>
    <?php
        /*
            Crea un formulario que puedas subir un archivo. En la pagina de
            destino ha de verse los atributos del archivo: nombre, tamaño,
            ubicación temporal, extensión…
        */
        // Comprobamos si ha introducido archivos
        if (isset($_FILES["archivo"]) && $_FILES["archivo"]["error"][0] == 0) {
          // Recorremos el array de archivos
          // Recorremos a partir de un campo ($_FILES["archivo"]["name"] por ejemplo) para que el bucle itere solo
          // por el numero de archivos
          // Si recorremos por $_FILES["archivo"], recorrera por las 5 propiedades de $_FILES
          foreach ($_FILES["archivo"]["name"] as $key => $archivo) {
            echo "<h4>Archivo ". ($key + 1). "</h4>";
            echo "<p>Nombre: ". $_FILES["archivo"]["name"][$key] . "</p>";
            echo "<p>Tamaño: ". $_FILES["archivo"]["size"][$key] . "</p>";
            echo "<p>Tipo de archivo: ". $_FILES["archivo"]["type"][$key] . "</p>";
            echo "<p>Ubicación temporal: ". $_FILES["archivo"]["tmp_name"][$key] . "</p>";
          }
        } else {
          echo "<p>No se ha introducido ningun archivo</p>";
        }

    ?>
</body>
</html>