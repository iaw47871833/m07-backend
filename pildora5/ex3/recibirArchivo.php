<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 3 Ficheros</title>
</head>
<body>
    <h1>Ejercicio 3 Ficheros</h1>
    <?php
        /*
            Crea un programa que subamos mediante un formulario un archivo. En
            la pagina de respuesta se ha de mover el archivo a una carpeta en el
            servidor llamada “/subidas” y mostrar todos los archivos que existen en
            esa carpeta (tambien los que habian antes). Si ejecutas el programa varias
            veces deberian de aparecer los archivos que subistes en los otros formularios enviados. 
        */
        // Comprobamos que se ha enviado un archivo
        // Si existe el archivo y no muestra ningun error, lo mostramos
        // En el caso contrario advertimos al usuario
        if (isset($_FILES["archivo"]) && $_FILES["archivo"]["error"] == 0) {
            // Guardamos el archivo
            $nombreArchivo = $_FILES["archivo"]["name"];
            // Guardamos el directorio donde guardaremos los archivos
            $directorio = "subidas/";
            // Comprobamos is existe el directorio
            // Si no existe lo creamos
            if (!file_exists($directorio)) {
                mkdir($directorio, 0777);
            }
            // Creamos el path donde se guardara el archivo
            $path = $directorio . $nombreArchivo;
            // Movemos el archivo 
            if (move_uploaded_file($_FILES["archivo"]["tmp_name"], $path)) {
                echo "Se ha guardado el archivo correctamente<br>";
            } else {
                echo "No se ha podido guardar el archivo";
            }
            // Cambiamos al directorio de subidas
            chdir($directorio);
            // Listamos los archivos y los mostramos
            foreach(glob("*.*") as $archivo) {
              echo $archivo . "<br>";
              // filesize, filetype, ubication
              echo "<p>Tamany: ". filesize($archivo). "</p>";
              echo "<p>Tipus: ". filetype($archivo). "</p>";
            }
        } else {
            echo "No se ha subido ningun archivo";
        }
    ?>
</body>
</html>