<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 5 Ficheros</title>
</head>
<body>
    <h1>Ejercicio 5 Ficheros</h1>
    <?php
      /*
        Crear un formulario con el campo: nombre de fichero, por ejemplo: test.txt Al enviar el formulario buscara si en la carpeta “/subidas” existe ese archivo y mostrara por pantalla el contenido. 
      */
        // Guardamos los campos
        $nombre = $_POST["nombre"];
        // Añadimos la extension
        $nombre .= ".txt";
        // Cambiamos al directorio subidas
        chdir("./subidas");
        // Buscamos el archivo
        // En el caso de encontrarlo lo mostramos
        if (glob($nombre)) {
          // Abrimos el archivo
          $archivo = fopen($nombre, "r");
          while(!feof($archivo)) {
              $linea = fgets($archivo);
              echo $linea . "<br>";
          }
        } else {
            echo "No se ha encontrado el archivo ". $nombre;
        }
        // Cerramos el archivo
        fclose($archivo);
    ?>
</body>
</html>