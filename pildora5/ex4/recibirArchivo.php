<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 4 Ficheros</title>
</head>
<body>
    <h1>Ejercicio 4 Ficheros</h1>
    <?php
    /*
        Crear un formulario con tres campos: nobmre de fichero,
        linea 1 y linea 2. Al enviar se debe de crear un 
        [nombreFichero].txt en la carpeta “/subidas” con las
        Lineas 1 y Linea 2 escritas en él. 
    */
        // Guardamos los campos
        $nombre = $_POST["nombreArchivo"];
        $linea1 = $_POST["linea1"];
        $linea2 = $_POST["linea2"];
        var_dump($linea1);
        var_dump($linea2);
         // Guardamos el directorio donde guardaremos los archivos
         $directorio = "subidas/";
         // Comprobamos is existe el directorio
         // Si no existe lo creamos
         if (!file_exists($directorio)) {
             mkdir($directorio, 0777);
         }
         // Cambiamos al directorio de subidas
         chdir($directorio);
         // Abrimos el archivo
         $archivo = fopen($nombre . ".txt", "w+");
         // Si se ha creado correctamente escribimos las lineas
        if ($archivo) {
          fputs($archivo, $linea1. "\n");
          fputs($archivo, $linea2. "\n");
        } else {
            echo "no se ha creado el archivo";
        }
        // Cerramos el archivo
        fclose($archivo);
    ?>
</body>
</html>